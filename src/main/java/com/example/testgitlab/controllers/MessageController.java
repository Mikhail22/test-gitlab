package com.example.testgitlab.controllers;

import com.example.testgitlab.models.Message;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
//@RequestMapping("/api/v1/")
public class MessageController {
    @GetMapping
    List<Message> getMessages(){
       List<Message> list = new ArrayList<>();
       list.add(new Message(1,"Honda_2"));
       list.add(new Message(2,"Toyota_2"));
       list.add(new Message(3,"Nissan_2"));
       list.add(new Message(4,"Kia_2"));
       return list;
    }
}
