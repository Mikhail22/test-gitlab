package com.example.testgitlab.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Message {
    Integer id;
    String text;
}
