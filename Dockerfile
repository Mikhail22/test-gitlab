FROM openjdk:8-jdk-alpine
#ARG JAR_FILE=target/*.jar
COPY target/test-gitlab-0.0.1-SNAPSHOT.jar app.jar
#COPY ${JAR_FILE} app.jar
#COPY src/main/webapp webapp
ENTRYPOINT ["java","-jar","/app.jar"]